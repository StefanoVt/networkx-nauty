networkx-nauty
==============

Networkx-nauty is a python binding of the nauty graph isomorphism library.

Still in alpha status, the plan is to eventually extend networkx.OrderedGraph to 
avoid datastructure conversion between networkx and nauty

Loosely inspired by https://github.com/networkx/networkx-metis

##Installation

networkx-nauty uses Cython to generate the bindings

##License
networkx-nauty and Nauty are released under the Apache License 2.0. Please see
`LICENSE.txt` and `src/nauty26r11/COPYRIGHT` for further details