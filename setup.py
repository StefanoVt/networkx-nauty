from distutils.core import setup
from glob import glob

from Cython.Build import cythonize
from setuptools import Extension
sources = ["networkx_nauty/nauty_sparse.pyx"]+["src/nauty26r11/{}.c".format(fn) for fn in
                                               ["nauty", "nautil", "nausparse", "naugraph", "schreier", "naurng"]]
#glob("src/nauty26r11/*.c")
#sources.remove("src/nauty26r11/sorttemplates.c")
#sources.remove("src/nauty26r11/poptest.c")
#sources.remove("src/nauty26r11/naugstrings.c")
#sources.remove("src/nauty26r11/dreadnaut.c")
#sources.remove("src/nauty26r11/testg.c")
#sources.remove("src/nauty26r11/splay.c")
#sources.remove("src/nauty26r11/nautyex3.c")




print(sources)

setup(
    name= "networkx-nauty",
    ext_modules = cythonize([Extension("networkx_nauty.nauty_sparse",sources,
                                       include_dirs=["src/nauty26r11"],
                                        depends= glob("src/nauty26r11/*.h"))], gdb_debug=True),#, libraries=["nauty"])]),
    #libraries = [('nauty', {'sources': glob("src/nauty26r11/*.c"),
    #                        'depends': glob("src/nauty26r11/*.h"),
    #                        'include_dirs' : "src/nauty26r11"})]
)