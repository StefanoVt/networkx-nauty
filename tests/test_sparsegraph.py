from unittest import TestCase
from networkx_nauty.nauty_sparse import SparseGraph
import networkx as nx

class SparseGraphTest(TestCase):
    def test1(self):
        sg = SparseGraph()

    def test2(self):
        sg = SparseGraph(nx.complete_bipartite_graph(3,4))
        ng = sg.to_networkx()
        print(ng.number_of_nodes())
        print(ng.number_of_edges())
        print(ng.adj)
        cg = sg.get_canon()
        cng = cg.to_networkx()
        print(cng.number_of_nodes())
        print(cng.number_of_edges())
        print(cng.adj)

    def test3(self):
        sg1 = SparseGraph(nx.complete_bipartite_graph(3,4))
        sg2 = SparseGraph(nx.complete_bipartite_graph(4,3))
        sgc1 = sg1.get_canon()
        sgc2 = sg2.get_canon()
        self.assertEqual(sgc1, sgc2)

    def test4(self):
        sg = SparseGraph(nx.complete_bipartite_graph(3, 4))
        ng = sg.to_networkx()
        print(ng.number_of_nodes())
        print(ng.number_of_edges())
        print(ng.adj)
        cg = sg.get_canon([[0,1,2,3,4,5,6]])
        cng = cg.to_networkx()
        print(cng.number_of_nodes())
        print(cng.number_of_edges())
        print(cng.adj)

    def test5(self):
        sg1 = SparseGraph(nx.complete_bipartite_graph(3, 4))
        sg2 = SparseGraph(nx.complete_bipartite_graph(4, 3))
        sgc1 = sg1.get_canon()
        sgc2 = sg2.get_canon()
        self.assertEqual(hash(sgc1), hash(sgc2))
        diffgc = SparseGraph(nx.complete_graph(7)).get_canon()
        self.assertNotEqual(sgc1, diffgc)
        self.assertNotEqual(hash(sgc1), hash(diffgc))

    def test6(self):
        sg1 = SparseGraph(nx.complete_bipartite_graph(3, 4))
        print(sg1.get_orbits())

    def test7(self):
        for s in range(100):
            g = nx.erdos_renyi_graph(10,0.5,seed=s)
            cg = SparseGraph(g).get_canon().to_networkx()
            self.assertEqual(set(frozenset(e) for e in g.edges()), set(frozenset(e) for e in cg.edges()))

if __name__ == "__main__":
    import unittest
    unittest.main()
