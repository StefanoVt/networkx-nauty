
cdef extern from "nausparse.h":
    ctypedef struct sparsegraph:
        int nv
        int nde
        int* v
        int* d
        int* e
        size_t vlen,dlen,elen,wlen

    int WORDSIZE

    ctypedef int boolean
    boolean TRUE,FALSE

    ctypedef struct optionblk:
        boolean getcanon
        boolean digraph
        boolean defaultptn


    void DEFAULTOPTIONS_SPARSEGRAPH(optionblk)

    ctypedef struct statsblk:
        int errstatus

    const int MTOOBIG, NTOOBIG, CANONGNIL, NAUABORTED, NAUKILLED

    void SG_INIT(sparsegraph)
    void SG_ALLOC(sparsegraph, int n, int e, char[] failure_message)
    void SG_FREE(sparsegraph)

    void sparsenauty(sparsegraph *g, int *lab, int *ptn, int *orbits, optionblk *options, statsblk *stats,
                     sparsegraph *canong)

    int aresame_sg(sparsegraph *g1, sparsegraph *g2)
